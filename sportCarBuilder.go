package main

type sportCarBuilder struct {
	wheelType string
	doorType   string
	speedometer      int
}

func newSportCarBuilder() *sportCarBuilder {
	return &sportCarBuilder{}
}

func (b *sportCarBuilder) setWheelType() {
	b.wheelType = "Super Car Wheels for high speed"
}

func (b *sportCarBuilder) setDoorType() {
	b.doorType = "Premium iron door"
}

func (b *sportCarBuilder) setSpeedometer() {
	b.speedometer = 400
}

func (b *sportCarBuilder) getCar() car {
	return car{
		doorType:   b.doorType,
		wheelType: b.wheelType,
		speedometer:      b.speedometer,
	}
}
