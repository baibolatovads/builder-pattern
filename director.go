package main

type director struct {
	builder iBuilder
}

func NewDirector(b iBuilder) *director {
	return &director{
		builder: b,
	}
}

func (d *director) setBuilder(b iBuilder) {
	d.builder = b
}

func (d *director) buildCar() car {
	d.builder.setDoorType()
	d.builder.setWheelType()
	d.builder.setSpeedometer()
	return d.builder.getCar()
}
