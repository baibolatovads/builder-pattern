package main


type iBuilder interface {
	setWheelType()
	setDoorType()
	setSpeedometer()
	getCar() car
}

func GetBuilder(builderType string) iBuilder {
	if builderType == "automobile" {
		return &normalCarBuilder{}
	}
	if builderType == "sport" {
		return &sportCarBuilder{}
	}
	return nil
}
