package main

import "fmt"

func main() {

	normalCarBuilder := GetBuilder("automobile")
	sportCarBuilder := GetBuilder("sport")

	director := NewDirector(normalCarBuilder)
	automobile := director.buildCar()

	fmt.Printf("Automobile Door Type: %s\n", automobile.doorType)
	fmt.Printf("Automobile Wheels Type: %s\n", automobile.wheelType)
	fmt.Printf("Automobile Speedometer: %d\n", automobile.speedometer)

	director.setBuilder(sportCarBuilder)
	sportCar := director.buildCar()

	fmt.Printf("\nSport Car Door Type: %s\n", sportCar.doorType)
	fmt.Printf("Sport Car Wheels Type: %s\n", sportCar.wheelType)
	fmt.Printf("Sport Car Speedometer: %d\n", sportCar.speedometer)
}
