package main

type normalCarBuilder struct {
	wheelType string
	doorType   string
	speedometer int
}

func newNormalBuilder() *normalCarBuilder {
	return &normalCarBuilder{}
}

func (b *normalCarBuilder) setWheelType() {
	b.wheelType = "Basic Wheels"
}

func (b *normalCarBuilder) setDoorType() {
	b.doorType = "Iron Door"
}

func (b *normalCarBuilder) setSpeedometer() {
	b.speedometer = 200
}

func (b *normalCarBuilder) getCar() car {
	return car{
		doorType:   b.doorType,
		wheelType: b.wheelType,
		speedometer:      b.speedometer,
	}
}
